import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';

import mainReducer from './reducers/index';
// const initialState = {};
const middleware = [thunk];

// LOCALSTORAGE
let initialState = localStorage.getItem( 'citas' );
if(initialState) {
    initialState = JSON.parse( initialState );
}
else {
    initialState = [];
}
// LOCALSTORAGE

const store = createStore( mainReducer, initialState, 
    compose(applyMiddleware(...middleware), 
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
) );

export default store;