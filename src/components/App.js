import React, { Component } from 'react';
import Header from './Header';
import Formulario from './Formulario';
import ListaCita from './ListaCita';

import {Provider} from 'react-redux';
import store from '../store';

class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <div className="container">
          <Header titulo='Gestor de citas' />
          <div className='row'>
            <div className='col-md-6'>
              <Formulario />
            </div>  
            <div className='col-md-6'>
              <ListaCita />
            </div>
          </div>
        </div>
      </Provider>
    );
  }

  componentDidMount() {
    const citas = localStorage.getItem( 'citas' );
    if(citas) {
      this.setState( {
        citas: JSON.parse( citas )
      } );
    }
  }
  componentDidUpdate() {
    localStorage.setItem(
      'citas',
      JSON.stringify( this.state.citas )
    );
  }

}

export default App;
