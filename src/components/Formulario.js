import React, { Component } from 'react';
import uuid from 'uuid';

import {connect} from 'react-redux';
import {addCita} from '../actions/citasAction';
import {mostrarError} from '../actions/errorAction';

class Formulario extends Component {

    mascota = React.createRef();
    duenio = React.createRef();
    fecha = React.createRef();
    hora = React.createRef();
    sintomas = React.createRef();

    agregarCita = (e) => {
        e.preventDefault();
        let mascota  = this.mascota.current.value.charAt(0).toUpperCase() + (this.mascota.current.value.slice(1)).toLowerCase();
        let duenio   = this.duenio.current.value.charAt(0).toUpperCase() + (this.duenio.current.value.slice(1)).toLowerCase();
        let fecha    = this.fecha.current.value;
        let hora     = this.hora.current.value;
        let sintomas = this.sintomas.current.value.charAt(0).toUpperCase() + (this.sintomas.current.value.slice(1));
        if ( mascota === '' || duenio === '' || fecha === '' || hora === '' || sintomas === '') {
            this.props.mostrarError(true);
        }
        else{
            let cita = {
                id: uuid(), mascota, duenio, fecha, hora, sintomas
            }
            this.props.addCita( cita );
            this.props.mostrarError(false);
            e.currentTarget.reset();
        }
    }

    componentWillMount() {
        this.props.mostrarError(false);
    }

    render() {
        const existeError = this.props.error;
        return (
            <div className='card mt-5'>
                <div className='card-body'>
                    <h2 className='card-title text-center mb-5'> Citas </h2>
                    <form onSubmit={this.agregarCita}>
                        <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form-label">Nombre Mascota</label>
                            <div className="col-sm-8 col-lg-10">
                                <input ref={this.mascota} type="text" className="form-control" placeholder="Nombre Mascota" />
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form-label">Nombre Dueño</label>
                            <div className="col-sm-8 col-lg-10">
                                <input ref={this.duenio} type="text" className="form-control"  placeholder="Nombre Dueño de la Mascota" />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form-label">Fecha</label>
                            <div className="col-sm-8 col-lg-4  mb-4 mb-lg-0">
                                <input ref={this.fecha} type="date" className="form-control" />
                            </div>                            

                            <label className="col-sm-4 col-lg-2 col-form-label">Hora</label>
                            <div className="col-sm-8 col-lg-4">
                                <input ref={this.hora} type="time" className="form-control" />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form-label">Sintomas</label>
                            <div className="col-sm-8 col-lg-10">
                                <textarea ref={this.sintomas} className="form-control" ></textarea>
                            </div>
                        </div>
                        <div className="form-group row justify-content-end">
                            <div className="col-sm-3">
                                <button type="submit" className="btn btn-success w-100">Agregar</button>
                            </div>
                        </div>
                    </form>
                    {existeError ? <div className='alert alert-danger text-center'> Ingresa campos solicitados </div> : ''}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {    
        citas: state.citas.citas,
        error: state.error.error
    }
}

export default connect(mapStateToProps, {
    addCita, mostrarError
})(Formulario);