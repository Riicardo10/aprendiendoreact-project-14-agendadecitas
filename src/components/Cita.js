import React, { Component } from 'react';

import {connect} from 'react-redux';
import {deleteCita} from '../actions/citasAction';

class Cita extends Component {

    eliminarCita = () => {
        this.props.deleteCita( this.props.informacion.id );
    }

    render() {
        const {mascota, duenio, fecha, hora, sintomas}  = this.props.informacion;
        return (
            <div className='media mt-3'>
                <div className='media-body'>
                    <h4 className='mt-0'> Mascota: {mascota} </h4>
                    <p className='card-text'> <span> Dueño: </span> {duenio} </p>
                    <p className='card-text'> <span> Fecha: </span> {fecha}  </p>
                    <p className='card-text'> <span> Hora: </span>   {hora}  </p>
                    <p className='card-text'> <span> Sintomas: </span> </p>
                    <p className='card-text'> {sintomas} </p>
                    <button onClick={this.eliminarCita} className='btn btn-danger'> Eliminar </button>
                </div>
            </div>
        );
    }
}

export default connect(null, {deleteCita}) (Cita);