import React, { Component } from 'react';
import Cita from './Cita';

import {connect} from 'react-redux';
import {getCitas} from '../actions/citasAction';

import store from '../store';

store.subscribe( () => {
    localStorage.setItem('citas', JSON.stringify( store.getState() ));
} );

class ListaCita extends Component {

    componentDidMount() {
        this.props.getCitas();
    }

    render() {
        const mensaje = this.props.citas.length === 0 ? 'Sin citas' : 'Listado de citas'
        return (
            <div className='card mt-5'>
                <div className='card-body'>
                    <h2 className='card-title text-center'> {mensaje} </h2>
                    <div className='lista-citas'>
                        { Object.keys( this.props.citas ).map( cita => {
                            return <Cita 
                                        key={cita}
                                        informacion={this.props.citas[cita]} />
                        } ) }
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {citas: state.citas.citas}
}

export default connect(mapStateToProps, {
    getCitas
})(ListaCita);