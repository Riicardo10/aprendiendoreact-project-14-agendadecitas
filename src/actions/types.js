export const MOSTRAR_CITAS  = 'MOSTRAR_CITAS';
export const AGREGAR_CITA   = 'AGREGAR_CITA';
export const ELIMINAR_CITA  = 'ELIMINAR_CITA';

export const VALIDAR_CITA   = 'VALIDAR_CITA';
export const MOSTRAR_ERROR  = 'MOSTRAR_ERROR';